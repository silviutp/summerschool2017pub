#pragma once

namespace Ex
{
    // returns the length of the input C string
    // The length of a C string is determined by the terminating null-character: 
    size_t strlen ( const char * str );

    // Compares the C string str1 to the C string str2.
    // returns 0 if the strings are equal
    // returns < 0 if str1 < str2 or > 0 otherwise
    // higher or lower for strings is considered using the characters ascii code
    int strcmp ( const char * str1, const char * str2 );

    // Copies the C string pointed by source into destination,
    // including the terminating null character (and stopping at that point).
    char * strcpy ( char * destination, const char * source );
    
    // Concatenates strings
    // Appends a copy of the source string to the destination string.
    // The terminating null character in destination is overwritten 
    // by the first character of source, and a null-character is included
    // at the end of the new string formed by the concatenation of both in destination.
    // destination and source shall not overlap.
    char * strcat ( char * destination, const char * source );
}
