# coding=utf-8
import urllib2
import sys

def replaceDiacritics(str):
	ah = '%C4%83' # ă
	ih = '%C3%A2' # â
	ii = '%C3%AE' # î
	sh = '%C8%99' # ș
	tz = '%C8%9B' # ț
	str = str.replace(ah,'a')
	str = str.replace(ih,'a')
	str = str.replace(ii,'i')
	str = str.replace(sh,'s')
	str = str.replace(tz,'t')
	return str

# Check if a word exists in dexonline
def main ():
	if len(sys.argv) <> 2:
		print 'Usage: CheckWord <word>'
		return -1 
	
	try:
		requestUrl = 'https://dexonline.ro/definitie/'+sys.argv[1]
		response = urllib2.urlopen(requestUrl)
		#Do not accept redirects except for diacritics change only
		responseUrl = response.geturl()
		responseUrl = replaceDiacritics(responseUrl)
		# TODO: maybe handle redirects with small (1-2 letters) changes
		if responseUrl != requestUrl:
			print 'Word not found'
			return -2
			
		page = response.read()
		searchedStr = "nu este în dicționar"
		if searchedStr in page:			
			print 'Word not found'
			return -2
	except Exception as e:
		#print e
		print 'An exception has occurred when accessing dexonline'
		return -3
	
	print 'Found'
	return 0

if __name__ == "__main__":
    rc = main ()
    sys.exit(rc)