#pragma once

namespace Test
{
    bool ExposeSecretKey();

    bool SwitchChar();

    bool WordExistsInDexonline();

    bool GenerateRandomLetter();

    bool IsValidWord();

    bool Str2Num();

    bool GenerateWord();

    bool GenerateValidWord();

    bool CountChars();
      
    bool CopyRange();

    bool SeparateWords();

    bool RunAllTests();
}