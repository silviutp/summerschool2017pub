#include "Tests.h"
#include "Exercises_Pointers.h"
#include "Util.h"
#include <cstring>
#include <iostream>

namespace
{
    template <typename TestFunc, typename... Args>
    bool DisplayTestInfo(TestFunc* testFunc, Args... args)
    {
        bool success = testFunc != nullptr && testFunc(args...);
        const char* result = " <Success>\n";
        if (!success)
            result = " <Failed>\n";
        std::cout << result;
        return success;
    }
}

namespace Test
{
#define PrintTestInfo std::cout<<"Running test "<<__FUNCTION__

    bool ExposeSecretKey()
    {
        PrintTestInfo;
        return strcmp(Ex::ExposeSecretKey(), "This is a secret.") == 0;
    }

    bool SwitchChar()
    {
        bool passes = Ex::SwitchChar(nullptr) == -1;
        passes = passes && Ex::SwitchChar("opt1") == 0;
        passes = passes && Ex::SwitchChar("opt2") == 1;
        passes = passes && Ex::SwitchChar("opt3") == 2;
        passes = passes && Ex::SwitchChar("opt4") == 3;
        passes = passes && Ex::SwitchChar("opt5") == 3;
        char opt[] = "opt";
        for (int i = 0; i < 255; ++i)
        {
            opt[0]++;
            passes = passes && Ex::SwitchChar("opt") == 3;
        }
        return passes;
    }

    bool WordExistsInDexonline()
    {
        PrintTestInfo;
        bool passes = true;
        passes = passes && !Util::WordExistsInDexonline("mrt");
        passes = passes && Util::WordExistsInDexonline("mort");
        passes = passes && Util::WordExistsInDexonline("cuveta");
        passes = passes && !Util::WordExistsInDexonline("cveta");
        return passes;
    }

    bool GenerateRandomLetter()
    {
        PrintTestInfo;
        bool passes = true;
        size_t iter = 0;
        // test if in [a-z] range;
        while (iter++ < 1000)
        {
            char c = Util::GenerateRandomLetter();
            if (c < 97 || c >122)
            {
                passes = false;
                break;
            }
        }
        return passes;
    }

    bool IsValidWord()
    {
        PrintTestInfo;
        bool passes = true;
        passes = passes && !Ex::IsValidWord("mrt",3);
        passes = passes && Ex::IsValidWord("mort",4);
        passes = passes && !Ex::IsValidWord("mort", 3);
        passes = passes && Ex::IsValidWord("cuveta",6);
        passes = passes && !Ex::IsValidWord("cuveta", 5);
        passes = passes && !Ex::IsValidWord("cveta",5);
        return passes;
    }

    bool GenerateWord()
    {
        PrintTestInfo;
        bool passes = true;
        char* word = Ex::GenerateWord(5);
        passes = passes && word != nullptr && strlen(word) == 5;
        free(word);
        return passes;
    }

    bool GenerateValidWord()
    {
        PrintTestInfo;
        bool passes = true;
        char* word = Ex::GenerateValidWord(3);
        passes = passes && word != nullptr && Util::WordExistsInDexonline(word);
        return passes;
        
    }

    bool CountChars()
    {
        PrintTestInfo;
        bool passes = true;
        passes = passes && Ex::CountChars("abcc", 'c') == 2;
        passes = passes && Ex::CountChars("ab", 'c') == 0;
        passes = passes && Ex::CountChars("abaa", 'a') == 3;
        passes = passes && Ex::CountChars("", 'a') == 0;
        passes = passes && Ex::CountChars(nullptr, 'a') == -1;
        return passes;
    }

    bool CopyRange()
    {
        PrintTestInfo;
        char* destination = nullptr;
        bool passes = true;
        
        int rc = Ex::CopyRange(destination, "abcd", 0, 2);
        passes = passes && rc == 0 && destination != nullptr && strcmp(destination, "abc") == 0;
        free(destination);
        if (!passes)
        {
            std::cout << " <bad output> ";
            return passes;
        }

        rc = Ex::CopyRange(destination, "abcd", 0, 4);
        passes = passes && rc != 0 && destination == nullptr;
        free(destination); destination = nullptr;
        if (!passes)
        {
            std::cout << " <maxIndex out of range> ";
            return passes;
        }

        rc = Ex::CopyRange(destination, "abcd", -1, 2);
        passes = passes && rc != 0 && destination == nullptr;
        free(destination); destination = nullptr;
        if (!passes)
        {
            std::cout << " <minIndex out of range> ";
            return passes;
        }
        rc = Ex::CopyRange(destination, "abcd", 3, 2);
        passes = passes && rc != 0 && destination == nullptr;
        free(destination); destination = nullptr;
        if (!passes)
        {
            std::cout << " <maxIndex lower than minIndex> ";
            return passes;
        }

        rc = Ex::CopyRange(destination, "abcd", 2, 2);
        passes = passes && rc == 0 && destination != nullptr && strcmp(destination, "c") == 0;
        free(destination);
        if (!passes)
        {
            std::cout << " <bad output> ";
            return passes;
        }

        return passes;        
    }

    namespace
    {
        void FreeArray(char**& words, size_t numWords)
        {
            if (words == nullptr)
                return;
            for (size_t i = 0; i < numWords; ++i)
            {
                free(words[i]);
            }
            free(words);
            words = nullptr;
        }
    }

    bool SeparateWords()
    {
        PrintTestInfo;
        char** words = nullptr;
        size_t numWords = 0;
        int rc = Ex::SeparateWords("unu doi trei", words, numWords);
        bool passes = rc == 0 && words != nullptr;
        if (passes)
        {
            passes = passes && numWords == 3;
            passes = passes && words[0] != nullptr && strcmp(words[0], "unu") == 0;
            passes = passes && words[1] != nullptr && strcmp(words[1], "doi") == 0;
            passes = passes && words[2] != nullptr && strcmp(words[2], "trei") == 0;
        }
        FreeArray(words, numWords); numWords = 0;

        rc = Ex::SeparateWords(" unu doi", words, numWords);
        passes = passes && rc == 0 && words != nullptr;
        if (passes)
        {
            passes = passes && numWords == 2;
            passes = passes && words[0] != nullptr && strcmp(words[0], "unu") == 0;
            passes = passes && words[1] != nullptr && strcmp(words[1], "doi") == 0;
        }
        FreeArray(words, numWords); numWords = 0;

        rc = Ex::SeparateWords("zero unu ", words, numWords);
        passes = passes && rc == 0 && words != nullptr;
        if (passes)
        {
            passes = passes && numWords == 2;
            passes = passes && words[0] != nullptr && strcmp(words[0], "zero") == 0;
            passes = passes && words[1] != nullptr && strcmp(words[1], "unu") == 0;
        }
        FreeArray(words, numWords); numWords = 0;

        rc = Ex::SeparateWords(" unu ", words, numWords);
        passes = passes && rc == 0 && words != nullptr;
        if (passes)
        {
            passes = passes && numWords == 1;
            passes = passes && words[0] != nullptr && strcmp(words[0], "unu") == 0;            
        }
        FreeArray(words, numWords); numWords = 0;

        return passes;
    }

    bool Str2Num()
    {
        PrintTestInfo;
        bool passes = true;
        int out = -1;
        passes = passes && Ex::Str2Num("123", out) == 0 && out == 123;
        passes = passes && Ex::Str2Num("-123", out) == 0 && out == -123;
        passes = passes && Ex::Str2Num(" -123", out) == 0 && out == -123;
        passes = passes && Ex::Str2Num("", out) != 0 ;
        passes = passes && Ex::Str2Num(nullptr, out) != 0;
        passes = passes && Ex::Str2Num("0123", out) == 0 && out == 123;
        passes = passes && Ex::Str2Num(" 123 ", out) == 0 && out == 123;
        passes = passes && Ex::Str2Num("12 3", out) == 0 && out == 123;

        return passes;
    }

    bool RunAllTests()
    {
        bool pass = true;
#define add(func) pass = DisplayTestInfo(func);
        add(SwitchChar);
        add(WordExistsInDexonline);
        add(ExposeSecretKey);
        add(GenerateRandomLetter);
        add(Str2Num);
        add(IsValidWord);
        add(GenerateValidWord);
        add(GenerateWord);
        add(CountChars);
        add(CopyRange);
        add(SeparateWords);
        // add new funcs to test here
#undef add

        return pass;
    }

#undef PrintTestInfo
}