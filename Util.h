#pragma once

namespace Util
{
    bool WordExistsInDexonline(const char* word);

    // returns the time [seconds] consumed by a function that runs for numRuns times
    template < size_t numRuns, typename Func, typename... Args>
    double MeasureTime(Func f, Args... args)
    {
        using namespace std;
        clock_t begin = clock();

        for (size_t i = 0; i < numRuns; ++i)
            f(args...);

        clock_t end = clock();

        return double(end - begin) / CLOCKS_PER_SEC;
    }

    // generates a random lowercase letter [a-z]
    // may pass a str with letters to exclude , e.g. "kqwxy" have a very low frequency in romanian
    char GenerateRandomLetter(const char* excludedLetters = "hjxykqw");
}