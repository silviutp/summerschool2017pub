
#include <array>
#include <cstdio>
#include <cstring>
#include <map>
#include <memory>
#include <random>
#include <string>
#include <stdexcept>


namespace Util
{
    // checks if a given word is available in dexonline.ro
    bool WordExistsInDexonline(const char* word)
    {        
        static const char* cmd = "public\\CheckWord\\CheckWord.exe "; // specify correct path here, otherwise will throw
        const size_t bufSize = 128;
        const size_t cmdSize = strlen(cmd);
        std::array<char, bufSize> fullCmd;
        strcpy(&fullCmd[0], cmd);
        strcpy(&fullCmd[cmdSize], word);

        std::shared_ptr<FILE> pipe(_popen((const char*)(&fullCmd[0]), "r"), _pclose);
        if (pipe == nullptr)
            return false;

        std::array<char, bufSize> buffer;
        std::string result;
        while (!feof(pipe.get()))
        {
            if (fgets(buffer.data(), bufSize, pipe.get()) != nullptr)
            {
                result += buffer.data();
            }
        }
        if (result.empty()) 
            throw std::runtime_error("WordExistsInDexonline: cmd is probably wrong, check that CheckWord.exe is in %PATH%") ;
        
        return result.compare("Found\n") == 0;
    }

    char GenerateRandomLetter(const char* excludedLetters)
    {        
        static std::random_device rd;
        static std::mt19937 mt(rd());
        std::uniform_real_distribution<double> dist(0, 100);

        // http://www.sepln.org/revistaSEPLN/revista/26/vlad.pdf
        // https://commons.wikimedia.org/wiki/File:Frecven%C8%9Ba_literelor_rom%C3%A2n%C4%83.png
        static std::map<char, double> letterFreq = {             
            { 'e' , 11.47 },
            { 'i' , 9.96 + 1.40 }, // i + ih
            { 'a' , 9.95 + 4.06 + 0.91 },// a + ah + ih
            { 'r' , 6.82 },
            { 'n' , 6.47 },
            { 'u' , 6.20 },
            { 't' , 6.04 + 1.00 },
            { 'c' , 5.28 },
            { 'l' , 4.48 },
            { 's' , 4.40 + 1.55 },
            { 'o' , 4.07 },
            { 'd' , 3.45 },
            { 'p' , 3.18 },
            { 'm' , 3.10 },
            { 'v' , 1.23 },
            { 'f' , 1.18 },
            { 'b' , 1.07 },
            { 'g' , 0.99 },
            { 'z' , 0.71 },
            { 'h' , 0.47 },
            { 'j' , 0.24 },
            { 'x' , 0.11 },
            { 'k' , 0.11 },
            { 'y' , 0.07 },
            { 'w' , 0.03 },
            { 'q' , 0.00 },
        };

        // TODO:  http://dexonline.net/litera/v could create different method for first letter
        
        // could extract this to a method
        auto generateLetter = [&]() {
            double generatedVal = dist(mt);
            double currentVal = 0.0;
            for (auto& mapEntry : letterFreq)
            {
                currentVal += mapEntry.second;
                if (generatedVal < currentVal)
                    return mapEntry.first;
            }
            return 'e';
        };

        char letter = generateLetter();
        if (excludedLetters != nullptr) // we could get rid of excludedLetters as it's not so useful now
        {
            // make this safe in case someone tries to exclude all letters
            if (std::strlen(excludedLetters) >= 26)
                letter = 'e';

            while (std::strchr(excludedLetters, letter) != nullptr)
                letter = generateLetter();
        }

        return letter;
    }
}
